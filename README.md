# Remix The School (Distributed)

**Remix The School Distributed** **Map** visualitza la xarxa de comunitats de pràctica que acceleren cap a la circularitat a través de l'educació STEAM, l'aprenentatge per projectes maker i els biomaterials.

[Fes click aquí per navegar sobre el mapa.](https://xavidominguez.gitlab.io/remix-the-school-distributed/)

![](https://i.imgur.com/GOnMqCc.png)

Remix The School (Distributed) és un programa de [Fab Lab Barcelona](https://fablabbcn.org/) amb el suport del [Consulat General dels Estats Units a Barcelona](https://es.usembassy.gov/es/embassy-consulates-es/barcelona-es/)

Per qualsevol dubte podeu contactar amb [xavi@fablabbcn.org](mailto://xavi@fablabbcn.org) o [santi@fablabbcn.org](mailto://santi@fablabbcn.org)
