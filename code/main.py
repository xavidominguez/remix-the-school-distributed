# Remix The School map generator
# by Xavi Domínguez
# powered by Fab Lab Barcelona

import folium
import csv
import pandas as pd

from folium.plugins import MarkerCluster
from IPython.display import HTML


nom=[]
data_lon=[]
data_lat=[]
comentaris=[]
color=[]
area=[]

# Load the data
y=0
with open("rtsd_comunitatp.csv") as rtsd:
    read=csv.reader(rtsd,delimiter=",")
    for i in read:
            nom.append(i[0])
            comentaris.append(i[8])
            data_lon.append(i[6])
            data_lat.append(i[7])
            color.append(i[3])
            area.append(i[2])


#for skiping columns name
nom=nom[1:]
data_lon=data_lon[1:]
data_lat=data_lat[1:]
comentaris=comentaris[1:]
color=color[1:]
area=area[1:]

map = folium.Map([41.4506304,2.0913448], zoom_start=11,tiles="Stamen Toner")
tile = folium.TileLayer('Stamen Terrain').add_to(map)

ly1 = MarkerCluster(name='Xarxa Remix The School').add_to(map)


#adding marker and popup of each Remix The School stakeholder
for i in range(0,len(nom)):
    folium.Marker(location=[float(data_lon[i]),float(data_lat[i])], icon=folium.Icon(color=""+color[i]),popup="<b>"+nom[i]+"</b>\n"+comentaris[i]).add_to(ly1)

#we can change tiles with help of LayerConrol
folium.LayerControl().add_to(map)

#saving map to a html file
map.save('index.html')
